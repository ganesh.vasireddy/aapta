import { Box } from '@mui/material';
import React, { useState } from 'react';
import Insights from './components/Insights';
import Sidenav from './components/Sidenav';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './components/Home';
import NewStandup from './components/NewStandup';

import EditPage from './components/EditPage';



function App() {
  const [heading, setHeading] = useState("My Standups")
  return (
    <div className="App">
      <Sidenav heading={heading} />
      
      <Box sx={{ alignItems: 'center', display: 'flex', flexWrap: "wrap", marginTop: 10, marginLeft: 10, gap: '1.5em' }}>
        
          <Routes>
            <Route path='/' element={<Home setHeading={(data) => setHeading(data)}  />} />
            <Route path='/insights' element={<Insights setHeading={(data) => setHeading(data)} />} />
            <Route path='/newstandup' element={<NewStandup setHeading={(data) => setHeading(data)} />} />
            <Route path='/editpage' element={<EditPage setHeading={(data) => setHeading(data)} />} />
            
          </Routes>
        
      </Box>

    </div>
  );
}

export default App;
