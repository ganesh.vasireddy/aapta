import React from 'react';
import { Card, CardContent,  Typography } from '@mui/material'
import { Box ,Grid} from '@material-ui/core'
import TextField from '@mui/material/TextField';
import EditIcon from '@mui/icons-material/Edit';


function Channels() {
  return (
    
    <Card sx={{width:'30%',background:'#F0F0F0',color:'black',marginTop:'20px',height:'110px'}}>
        <CardContent>
    <Grid container my={1} sx={{display:'flex',justifyContent:'space-between'}}>
        <Grid item xs = '11'  ><Typography >Channels</Typography></Grid>
        
    </Grid>
    <TextField sx={{width:'100%',fontFamily: 'Inter',fontStyle: 'normal',fontWeight:'400',fontSize:'30px',lineHeight:'36px'}} id="outlined-basic" label="#" variant="outlined" />

    </CardContent>
    </Card>
    
    
  )
}

export default Channels;
