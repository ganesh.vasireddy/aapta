import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';



export default function ListOfStandups() {
  return (
    <Card sx={{ minWidth: 200, minHeight:450,background:'#F0F0F0'}}>
      <CardContent sx={{padding:'25px'}}>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          <Typography sx={{fontSize:'20px',alignContent:'center',marginBottom:'10px'}}>Standups</Typography>
        <div style={{display:'list-item',listStylePosition: 'inside',fontSize:16}}>Dev Scrum</div>
        <div style={{display:'list-item',listStylePosition: 'inside',fontSize:16}}>Prod Scrum</div>
        <div style={{display:'list-item',listStylePosition: 'inside',fontSize:16}}>CSM Scrum</div>
        </Typography>
        
      </CardContent>
      
    </Card>
  );
}
