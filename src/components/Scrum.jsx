import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Grid } from '@mui/material';
import Box from '@mui/material/Box';
import SettingsIcon from '@mui/icons-material/Settings';
import { Link } from 'react-router-dom';

export default function Scrum() {
  return (
    <Link to="/insights" style={{textDecoration:'none'}}>
      <Card sx={{
         textDecoration:'none',minHeight: '280px', minWidth: '400px', display: 'flex', margin: "8px", background: '#002845', boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)', borderRadius: '35px', left: '344px',
        top: '100px'
      }}>

        <CardContent sx={{ alignItems: 'top',width:'100%' }}>
          <Box style={{ display: 'flex', justifyContent: 'space-between'}}>

            <Typography   sx={{color:'white'}} gutterBottom variant="h5" component="div">
              Dev Scrum
            </Typography>
            <Grid item xs={8}  >
            <Link to="/editpage" style={{color:"white"}}> <SettingsIcon /></Link>
            </Grid>
          </Box>
          <Typography sx={{color:'white'}} variant="body2" color="text.secondary">
            schedule on 10 Am everyday
          </Typography>

        </CardContent>

      </Card>
    </Link>
  );
}