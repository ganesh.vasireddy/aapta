import React from "react";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

export default function Heading() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar style={{ background: '#CDE2F5',minHeight:70 }} position="fixed">
        <Toolbar  sx={{ justifyContent: "space-between" }}>
          <Typography variant="h6"  style={{ color: '#000000',fontSize: 26,fontWeight: 'bold', }}component="div">
            My Standups
          </Typography>
          <Button style={{ background: '#FF7F33',borderRadius:40 }}color="inherit">+ New Standup</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}


