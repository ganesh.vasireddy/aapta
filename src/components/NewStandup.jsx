import { Box } from '@mui/system'
import React, { useEffect } from 'react'
import AddParticipants from './AddParticipants';
import Channels from './Channels';
import Description from './Description'
import Schedule from './Schedule';







const NewStandup = ({setHeading}) => {
    useEffect(() => {
        setHeading("Standup Name")
    }, [])
  return (  
    <Box sx={{width:'100%'}}>
    <Description/>  
    <Box sx={{display:'flex',width:'60%'}}>
    <Schedule /> 
    <Channels />
    <AddParticipants/>
    </Box>
    </Box>
  )
}

export default NewStandup