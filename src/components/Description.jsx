import React from 'react'

import { Card, CardContent,  Typography } from '@mui/material'
import { Box ,Grid} from '@material-ui/core'
import TextField from '@mui/material/TextField';
import EditIcon from '@mui/icons-material/Edit';


function Description() {
  return (
    
    <Card sx={{width:'90%',minHeight:'15px', background:'#F0F0F0',color:'black'}}>
        <CardContent>
    <Grid container my={1} sx={{display:'flex',justifyContent:'space-between'}}>
        <Grid item xs = '11'  ><Typography >Description</Typography></Grid>
        <Grid item xs='1' sx={{padding  :'30px'}} ><EditIcon /></Grid>
    </Grid>
    <TextField sx={{width:'100%',fontFamily: 'Inter',fontStyle: 'normal',fontWeight:'400',fontSize:'30px',lineHeight:'36px'}} id="outlined-basic" label="description of that particular scrum" variant="outlined" />

    </CardContent>
    </Card>
    
    
  )
}

export default Description;
