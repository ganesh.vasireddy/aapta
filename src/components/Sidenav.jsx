import React, { Profiler, useEffect, useMemo, useState } from "react";

import {
  AppBar,
  Toolbar,
  
  Box,
  
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  
  ListItemText,
  makeStyles,
  CssBaseline,
  Drawer,
  Typography
} from "@material-ui/core";
import {
  
  Menu,
  
  Home
} from "@material-ui/icons";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';

import Button from '@mui/material/Button';
import { useLocation, useNavigate } from "react-router-dom";
import { Link } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
  menuSliderContainer: {
    width: 150,
    background:'#002845',
    
    height: "100%"
  },
  
  listItem: {
    color: 'white'
  }
}));

const listItems = [
  {
    listIcon: <Home />,
    listText: "Home"
  },
  {
    listIcon: <AccountCircleIcon />,
    listText: "Profile"
  }
  
];

export default function Sidenav({heading}) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);


  const toggleSlider = () => {
    setOpen(!open);
  };

  const sideList = () => (
    <Box   className={classes.menuSliderContainer} component="div">
      <List >
        {listItems.map((listItem, index) => (
          <ListItem className={classes.listItem} button key={index}>
            <ListItemIcon className={classes.listItem}>
              {listItem.listIcon}
            </ListItemIcon>
            <ListItemText primary={listItem.listText} />
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <>
      <CssBaseline />

      <Box component="nav">
        <AppBar style={{background:'#CDE2F5'}} position="fixed">
          <Toolbar sx={{ justifyContent: "space-between"}} >
            <IconButton onClick={toggleSlider}>
              <Menu />
            </IconButton>
            
            <Typography style={{color: '#000000',fontSize: 26,fontWeight: 'bold',}}>{heading}</Typography>
            <Link to="/newstandup" style={{textDecoration:'none',marginLeft:'auto'}} ><Button style={{ background: '#FF7F33',borderRadius:40,marginLeft: "auto" ,color:'white',textTransform: 'capitalize',boxShadow:'10px'}} color="inherit">+ New Standup</Button></Link>
            
            <Drawer open={open} anchor="right"    onClose={toggleSlider}>
              {sideList()}
            </Drawer>
          </Toolbar>
        </AppBar>
      </Box>
    </>
  );
}
