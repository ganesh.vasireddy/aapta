import React from 'react';
import { Card, CardContent,  Typography } from '@mui/material'
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';


function AddParticipants() {

    const [age, setAge] = React.useState('');

  const handleChange = (event: SelectChangeEvent) => {
    setAge(event.target.value)
  };

  return (
    
    <Card sx={{width:'30%',background:'#F0F0F0',color:'black',marginTop:'20px',height:'110px'}}>
        <CardContent>
        <Typography >Participants</Typography>

        <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Select Participants</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={age}
          label="Age"
          onChange={handleChange}
        >
          
        </Select>
      </FormControl>
    </Box>
        
    </CardContent>
    </Card>
    
    
  )
}

export default AddParticipants;
