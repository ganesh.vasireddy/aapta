import Scrum from "./Scrum";
import React, { useEffect } from 'react'
import NewStandup from "./NewStandup";
import ListOfStandups from "./ListOfStandups";
import { Box } from "@mui/material"
export default function Home({setHeading}) {
    useEffect(() => {
        setHeading("My Standups")
    }, [])
    return (
       // <Box><
       <Box style={{display:"flex"}}>
       <Box style={{display:"flex",flexWrap:"wrap",justifyContent:'space-evenly'}}>
            <Scrum />
            <Scrum /> 
            <Scrum />
            <Scrum />
        </Box >
        <Box sx={{marginRight:'40px' ,marginTop:'20px'}}>
        <ListOfStandups />
        </Box>
        </Box>
    )
}